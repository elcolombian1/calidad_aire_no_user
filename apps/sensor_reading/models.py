from __future__ import unicode_literals
import json

from django.http import JsonResponse

from django.db import models

class dato(models.Model):
	dato_id=models.AutoField(primary_key=True)
	tipo_dato=models.CharField(max_length=20)
	valor_dato=models.FloatField()
	fecha=models.DateTimeField(auto_now_add=True)
	
	def __str__(self):
		return ("|| Tipo de dato: {} || Valor medido: {} || Fecha de medicion: {} || Hora de medicion: {} ||".format (self.tipo_dato,self.valor_dato,self.fecha.strftime("%Y-%m-%d"),self.fecha.strftime("%H:%M:%S")))
		##return("{}".format(self.valor_dato))
	def dato_get(self):
		return("{}".format(self.valor_dato))
	
	class Meta:
   		ordering = ['fecha']

