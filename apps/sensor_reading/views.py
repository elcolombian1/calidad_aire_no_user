# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render,redirect
from django.http import HttpResponse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.http import JsonResponse
from models import dato
import datetime
import os
from io import BytesIO
from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import A4,letter
import random
from django.core import serializers
import datetime
def data_request(req):
		
	now=datetime.datetime.now()
	temperatura=dato.objects.filter(tipo_dato="Temperatura").only("valor_dato").order_by('-dato_id')[0]
	humedad=dato.objects.filter(tipo_dato="Humedad").only("valor_dato").order_by('-dato_id')[0]
	co2=dato.objects.filter(tipo_dato="CO2").only("valor_dato").order_by('-dato_id')[0]
	o3=dato.objects.filter(tipo_dato="O3").only("valor_dato").order_by('-dato_id')[0]
	co=dato.objects.filter(tipo_dato="CO").only("valor_dato").order_by('-dato_id')[0]	
	nh3=dato.objects.filter(tipo_dato="NH3").only("valor_dato").order_by('-dato_id')[0]
	if req.method=='GET':
		print("Post")
		return JsonResponse({
			'Temp':str(temperatura.dato_get())+" C°",
			'ozone':str(o3.dato_get())+" PPM",
			'co2':str(co2.dato_get())+" PPM",
			'hum':str(humedad.dato_get())+" %",
			'nh3':str(nh3.dato_get())+" PPM",
			'co':str(co.dato_get())+" PPM",
			'no2':str(random.randint(1,21)*5)+" PPM",
			'Date':str(now.strftime("%Y-%m-%d %H:%M:%S"))})
	else :
		return JsonResponse({"foo":"No data found"})
	


def consulta(req):

	if 'year_start_session' not in req.session:
	
		req.session['year_start_session']="1990"
		req.session['month_start_session']="1"
		req.session['day_start_session']="1"

		req.session['year_finish_session']="1990"
		req.session['month_finish_session']="1"
		req.session['day_finish_session']="1"

		year_start=("1")
		month_start=("1")
		day_start=("1")

		year_finish=("1")
		month_finish=("1")
		day_finish=("1")

	date=req.GET.get("date_start")
	date_finish=req.GET.get("date_finish")

	if date:	
			print(date)

			year_start=int((date[0:4]))
			day_start=int((date[5:7]))
			month_start=int((date[8:10]))
			print(year_start)
			print(month_start)
			print(day_start)

			# year_start=str(year_start)
			# month_start=str(month_start)
			# day_start=str(day_start)

			year_finish=int((date_finish[0:4]))
			day_finish=int((date_finish[5:7]))
			month_finish=int((date_finish[8:10]))

			# year_finish =str(year_finish)
			# month_finish =str(month_finish)
			# day_finish =str(day_finish)

			print("{} Numero de datos".format(len(date)))	
			req.session['year_start_session']= year_start
			req.session['month_start_session']= month_start
			req.session['day_start_session']= day_start

			req.session['year_finish_session']= year_finish
			req.session['month_finish_session']= month_finish
			req.session['day_finish_session']= day_finish
	else:
		year_start=req.session['year_start_session']
		month_start=req.session['month_start_session']
		day_start=req.session['day_start_session']
		year_finish=req.session['year_finish_session']
		month_finish=req.session['month_finish_session']
		day_finish=req.session['day_finish_session']

	if "tipo_dato_session" not in req.session:
		req.session['tipo_dato_session']="Trigger"
	if "index_session" not in req.session:
		req.session['index_session']=8

	if req.method=="GET":
		if "index" in req.GET:
			index=req.GET.get("index")
			req.session['index_session']=index
		else:
			index=req.session['index_session']
		if "tipo_dato" in req.GET :
			tipo_dato_req=req.GET.get("tipo_dato")
			req.session['tipo_dato_session']=req.GET.get("tipo_dato")
		else:	
			tipo_dato_req=req.session['tipo_dato_session']
			
		if tipo_dato_req == "Todos" :
				
				data=dato.objects.filter(fecha__gte=datetime.date(year_start, day_start, month_start),fecha__lte=datetime.date(year_finish, day_finish, month_finish))
				print("{} Dia->>{} Mes->>{} Año ->>{}".format(data,day_start,month_start,year_start))
				
				count=(data.count())
		else:
				data=dato.objects.filter(tipo_dato=tipo_dato_req,fecha__gte=datetime.date(year_start, day_start, month_start),fecha__lte=datetime.date(year_finish, day_finish, month_finish))
				print("{} Dia->>{} Mes->>{} Año ->>{}".format(data,day_start,month_start,year_start))
				count=(data.count())
			

		paginator = Paginator(data, index) # Show 25 contacts per page

		try:
			page = int(req.GET.get('page'))
		except:
				page=1	

		try:
				
				data_page = paginator.page(page)


		except EmptyPage:
		        
				data_page = paginator.page(paginator.num_pages)

		
			
		if "reporte" in req.GET:
			now = datetime.datetime.now()
			response=HttpResponse(content_type='application/pdf')	
			response['Content-Disposition']='attachment;filename=Calidad_Aire_reporte '+str(now)+'.pdf'
			buffer=BytesIO()
			top_page=720
			page_number_pdf=0
			line_space=top_page-30
			data_count=1
			p=canvas.Canvas(buffer,pagesize=letter)
			p.setFont("Helvetica-Bold",12)
			p.drawString(30, top_page, "REPORTE DE CALIDAD DE AIRE :"+str(now.strftime("%Y-%m-%d %H:%M:%S")))
			for datos in data:
						print(datos)
						p.setFont("Helvetica",9)
						p.drawString(50, line_space, "Dato: "+str(data_count)+" "+str(datos))
						line_space-=30
						data_count+=1
						page_number_pdf+=1
						if page_number_pdf==20:
							p.showPage()
							page_number_pdf=0
							line_space=top_page-30
							p.setFont("Helvetica-Bold",12)
							p.drawString(30, top_page, "REPORTE DE CALIDAD DE AIRE :"+str(now.strftime("%Y-%m-%d %H:%M:%S")))
			p.save()
			pdf = buffer.getvalue()
			buffer.close()
			response.write(pdf)
			return response
		return render(req,"data_base.html",{"datos_list":data_page,"total_registros":count})


	






