from classes import save_data,sensor_reader       
import time			
from mq import *
import sys, time

def main():
		## def __init__(self,analogPin, offset,slope,const,Ro=10,):
		
		mq_CO2 = MQ(0,0,-2.802,127.8,10);
		mq_CO = MQ(0,0,-4.771,1254.4,1.5);
		mq_O3 = MQ(1,0,-1.0432297135,42.85561841,0.01);
		mq_NH3 = MQ(2,0,-1.73,0.7382,2);
		##Llama a la clase MQ para lectura de GAS con curva para CO2  y el pin del ADC y offset
		##mq_CO2 = MQ([2.63346845558,0.77815125038,1.387728],0,1);
		data_manage=save_data()#Inicializa objeto de la clase base de datos para alojar datos 
		read_sensor=sensor_reader()#inicializa objeto para lectura de datos
		while(True):
			try:	
				# Obtiene la humedad y la temperatura desde el sensor 
				CO2=mq_CO2.MQPercentage()
				CO=mq_CO.MQPercentage()
				O3=mq_O3.MQPercentage()
				NH3=mq_NH3.MQPercentage()
				humedad, temperatura =read_sensor.get_data_temp_humidity() 
				data_manage.save_temperature(temperatura)
				data_manage.save_humidity(humedad)
				data_manage.save_CO2(float('%.3f'% CO2["PPM_RESPONSE"]))##Guarda el valor de CO2 con cuatro cifras significativas.
				data_manage.save_CO(float('%.3f'% CO["PPM_RESPONSE"]))##Guarda el valor de CO2 con cuatro cifras significativas.
				data_manage.save_O3(float('%.3f'% O3["PPM_RESPONSE"]))##Guarda el valor de CO2 con cuatro cifras significativas.
				data_manage.save_NH3(float('%.3f'% NH3["PPM_RESPONSE"]))##Guarda el valor de CO2 con cuatro cifras significativas.

				
				
				print("{} <-- PPM (CO2) --- {} <-- PPM (CO)----- {} <-- PPM (O3) {} <---NH3".format(float('%.3f'%CO2["PPM_RESPONSE"]),float('%.3f'%CO["PPM_RESPONSE"]),float('%.3f'%O3["PPM_RESPONSE"]),float('%.3f'%NH3["PPM_RESPONSE"]))) 
				print("Funcionando")
				# Duerme 10 segundos
				time.sleep(0.5)
				
		# Se ejecuta en caso de que falle alguna instruccion dentro del try
			except Exception,e:
			# Imprime en pantalla el error e
				print str(e)	

		

if __name__=="__main__":
	main()
	



