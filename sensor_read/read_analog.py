import time
import Adafruit_GPIO.SPI as SPI
import Adafruit_MCP3008

CLK  = 11
MISO = 10
MOSI = 9
CS   = 8
mcp = Adafruit_MCP3008.MCP3008(clk=CLK, cs=CS, miso=MISO, mosi=MOSI)

print('Reading MCP3008 values, press Ctrl-C to quit...')

print("Reading channel 0")
while True:
	value=[]
	i=0
	value=(mcp.read_adc(2))
	print('|{} V'.format((value*3.3)/1024))
	time.sleep(0.5)
