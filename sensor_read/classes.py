import time 
import Adafruit_DHT 
import datetime 
import MySQLdb


class save_data(): ## Save Data
	def __init__(self):
			try:
				self.db = MySQLdb.connect("localhost","raspberry","root","Datos")
				self.cursor=self.db.cursor()	
				print("Conexion Exitosa")
		
			except:
				print("Conexion a base de datos fallida")

	def save_temperature(self,value):
		self.cursor.execute("INSERT INTO sensor_reading_dato (tipo_dato,valor_dato,fecha) VALUES ('Temperatura',%s,%s)",(value,datetime.datetime.now()))
		self.db.commit()

	def save_humidity(self,value):	
		self.cursor.execute("INSERT INTO sensor_reading_dato (tipo_dato,valor_dato,fecha) VALUES ('Humedad',%s,%s)",(value,datetime.datetime.now()))
		self.db.commit()
	
	def save_CO2(self,value):
		self.cursor.execute("INSERT INTO sensor_reading_dato (tipo_dato,valor_dato,fecha) VALUES ('CO2',%s,%s)",(value,datetime.datetime.now()))
		self.db.commit()
	def save_O3(self,value):
		self.cursor.execute("INSERT INTO sensor_reading_dato (tipo_dato,valor_dato,fecha) VALUES ('O3',%s,%s)",(value,datetime.datetime.now()))
		self.db.commit()
	def save_CO(self,value):
		self.cursor.execute("INSERT INTO sensor_reading_dato (tipo_dato,valor_dato,fecha) VALUES ('CO',%s,%s)",(value,datetime.datetime.now()))
		self.db.commit()	
	def save_NH3(self,value):
		self.cursor.execute("INSERT INTO sensor_reading_dato (tipo_dato,valor_dato,fecha) VALUES ('NH3',%s,%s)",(value,datetime.datetime.now()))
		self.db.commit()		

	def close_db(self):
		self.cursor.close()

class sensor_reader():
        
        def __init__(self):
		self.kind_sensor = Adafruit_DHT.DHT11 # Tipo de sensor de humedad usado
		self.temp_hum_pin = 21 ##Pin para recepcion de informacion desde pines del sensor de humedad
		print("{}".format(self.temp_hum_pin))
	def get_data_temp_humidity(self):##implemenatacion de metodo para obtener informacionde sensor de temperatura
                return(Adafruit_DHT.read_retry(self.kind_sensor,self.temp_hum_pin))
