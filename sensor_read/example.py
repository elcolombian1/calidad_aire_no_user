from mq import *
import sys, time

def main():
    print("Press CTRL+C to abort.")
    
    mq = MQ();
    while True:
        perc = mq.MQPercentage()
        print("{} <-- PPM (CO2)".format(perc["CO2"]))
        time.sleep(0.1)

if __name__=="__main__":
    main()
