# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.http import HttpResponse 

def index(req):
	req.session['tipo_dato_session']="Trigger"
	return render(req,"base.html")


def medicion(req):
	req.session['tipo_dato_session']="Trigger"
	return render(req,"data.html")